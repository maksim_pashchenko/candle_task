<?php

add_action('wp_enqueue_scripts', 'norebro_child_local_enqueue_parent_styles');

function norebro_child_local_enqueue_parent_styles()
{
    wp_enqueue_style('parent-style', get_template_directory_uri() . '/style.css');


//	if (is_front_page()) {
    wp_enqueue_script('custom-js', get_stylesheet_directory_uri() . '/custom-scripts/custom-script.js', array(), '1.0.0', true);
    wp_enqueue_style('custom-css', get_stylesheet_directory_uri() . '/custom-style/custom-style.css');

//    }


    wp_localize_script( 'custom-js', 'ajax',
        array(
            'url' => admin_url('admin-ajax.php'),
            'nonce' => wp_create_nonce('myajax-nonce')
        )
    );
}

add_action( 'admin_enqueue_scripts', 'admin_enqueue_script'  );

function admin_enqueue_script() {
    global $post_type;
    if( 'payments' == $post_type ) {
        wp_enqueue_script('payment_script', get_stylesheet_directory_uri() . '/custom-scripts/payment_admin.js');
    }
}

if( function_exists('acf_add_options_page') ) {
    acf_add_options_page();
}


//$yourfield = get_field('test', 'options');
//
//if($yourfield) :
//
//    echo $yourfield;
//
//endif;




add_action('wp_footer', 'popup_html');
function popup_html(){
     get_template_part('parts/price-table-popup');
};

function create_payments_post() {

    register_post_type( 'Payments',
        // CPT Options
        array(
            'labels' => array(
                'name' => __( 'Payments' ),
                'singular_name' => __( 'Payment' )
            ),
            'public' => true,
            'has_archive' => true,
            'supports' => array('title'),
            'rewrite' => array('slug' => 'payments'),
            'capabilities' => array(
                'create_posts' => false, // Removes support for the "Add New" function
            ),
            'map_meta_cap'       => true,
        )
    );
}
// Hooking up our function to theme setup
add_action( 'init', 'create_payments_post' );




add_action('wp_ajax_my_action', 'my_action_callback');
add_action('wp_ajax_nopriv_my_action', 'my_action_callback');
function my_action_callback() {

    check_ajax_referer( 'myajax-nonce', 'nonce_code' );

//    if( ! wp_verify_nonce( $_POST['nonce_code'], 'myajax-nonce' ) ) die( 'Stop!');

    $form_params = array();
    parse_str($_POST['data'], $form_params);


    $post_id = wp_insert_post([
        'post_type' => 'payments',
        'post_title' => $_POST['transaction_id'],
        'post_status' => 'publish',
    //            'post_content' => $charge->id
    ]);

    update_field('amount', $_POST['amount'], $post_id);
    update_field('business_about', $form_params['description'], $post_id);
    update_field('form_name', $form_params['name'], $post_id);
    update_field('form_email', $form_params['email'], $post_id);
    update_field('currency', $_POST['currency_code'], $post_id);
    update_field('payer_name', $_POST['payer_name'], $post_id);
    update_field('payer_email', $_POST['payer_email'], $post_id);
    update_field('payer_email', $_POST['payer_email'], $post_id);
    update_field('payer_surname', $_POST['payer_surname'], $post_id);

    $headers = 'From: <company@mydomain.com>' . "\r\n";
    $mail_content = $form_params['name'] . "\n" . $form_params['email'] . "\n" .  $form_params['description'];

    wp_mail(get_option('admin_email'), 'company name', $mail_content, $headers);


    wp_die();
}

