jQuery(function ($) {
    var price;
    var button;

    function showPricePopup() {
        $('.modal-price').fadeIn(400);

        $('.price-table-form__field').on('input', function () {
            statePayButton();
        });

        $('body').on('click', '.paypal-buttons', function () {
            $('.price-table-form__field').on('input', function () {
                checkFormFilling();
                statePayButton();
            });
        });

        $('.paypal-buttons').remove();

        paypal.Buttons({
            style: {
                color: 'blue'
            },
            createOrder: function (data, actions) {
                return actions.order.create({
                    purchase_units: [{
                        amount: {
                            value: price
                        }
                    }]
                });
            },
            onApprove: function (data, actions) {
                return actions.order.capture().then(function (details) {
                    var data = {
                        nonce_code: ajax.nonce,
                        action: 'my_action',
                        data: $(".price-table-form").serialize(),
                        transaction_id: details.id,
                        payer_name: details.payer.name.given_name,
                        payer_surname: details.payer.name.surname,
                        payer_email: details.payer.email_address,
                        amount: details.purchase_units[0].amount.value,
                        currency_code: details.purchase_units[0].amount.currency_code
                    };

                    jQuery.post(ajax.url, data, function (response) {
                        $('.modal-price').fadeOut(400);
                        $('.modal-thanks').fadeIn(400);
                    });
                });
            }
        }).render('.modal-price__body');

        disablePayButton();


    }

    function hidePricePopup(modal) {
        modal.closest('.modal').fadeOut(400);
        $('.price-table-form__field').off('input', checkFormFilling(), statePayButton())
    }

    function clearDataModal() {
        $('.price-table-form__field').each(function (key, value) {
            $(value).val('');
            $(value).siblings('.price-table-form-label__title_error').removeClass('price-table-form-label__title_error');
        });
    }


    function disablePayButton() {
        button = $('.paypal-buttons');
        if (button) {
            button.addClass('paypal-buttons_disabled');
        }
    }

    function isEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }

    function checkFormFilling() {
        disablePayButton();
        $('.price-table-form__field').each(function (key, value) {
            if (($(value).val().length < 2) || $(value).hasClass('price-table-form__email') && !isEmail($(value).val())) {
                $(value).siblings('.price-table-form-label__title').addClass('price-table-form-label__title_error');
            } else {
                $(value).siblings('.price-table-form-label__title').removeClass('price-table-form-label__title_error');
            }
        });
    }

    function statePayButton() {
        var empty = true;
        $('.price-table-form__field').each(function (key, value) {
            if (($(value).val().length < 2) || $(value).hasClass('price-table-form__email') && !isEmail($(value).val())) {
                empty = false;
            }
        });
        if (empty) {
            button.removeClass('paypal-buttons_disabled');
        } else {
            button.addClass('paypal-buttons_disabled');
        }
    }


    function getData(button) {
        var priceTable = button.closest('.pricing-table');

        price = priceTable.find('.price h2').text().replace('$', '');
        price = $.trim(price);
        var numberCompanies = priceTable.find('.list-box li').eq(0).find('.title').text().match(/\d+/);
        var title = priceTable.find('h3.title').text();
        var time = priceTable.find('.list-box li').eq(1).text().match(/\d+/);
        $('.modal-price-head__number-company').text(numberCompanies);
        $('.modal-price-head__type').text(title);
        $('.modal-price-head__time').text(time);
    }

    $('.pricing-table .btn-squared').on('click', function () {
        getData($(this));
        showPricePopup();
    });
    $(document).on('click', function (e) {
        if ($(e.target).hasClass('modal-content') || $(e.target).hasClass('modal__close') || $(e.target).hasClass('modal-body__ok')) {
            hidePricePopup($(e.target));
            clearDataModal();
        }
    });

    $('body').on('click', '.paypal-buttons', function () {
        checkFormFilling();
    });

});
