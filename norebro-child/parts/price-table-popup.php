<!--<div class="modal-overlay"></div>-->

<div class="modal modal-price">
    <div class="modal-container">
        <div class="modal-content">
            <div class="modal-body">
                <span class="modal__close">×</span>

                <div class="modal-price-head">
                    <p class="modal-price-head__title">The <span class="modal-price-head__type"></span> Plan.</p>
                    <p class="modal-price-head__subtitle">Almost there! </p>
                    <p class="modal-price-head__text">Receive <strong><span class="modal-price-head__number-company"></span> company name ideas within <span
                                    class="modal-price-head__time"></span> hours.</strong>
                    </p>
                </div>
                <div class="modal-price__body">
                    <form class="price-table-form" method="post" action="">
                        <div class="price-table-form__data price-table-form__group">
                            <div class="price-table-form__item">
                                <label class="price-table-form-label">
                                    <span class="price-table-form-label__title">Name:</span>
                                    <input type="text" name="name" class="price-table-form__field price-table-form__input">
                                </label>
                            </div>
                            <div class="price-table-form__item">
                                <label class="price-table-form-label">
                                    <span class="price-table-form-label__title">Email:</span>
                                    <input type="text" name="email"
                                           class="price-table-form__field price-table-form__email price-table-form__input">
                                </label>
                            </div>
                        </div>
                        <div class="price-table-form__data price-table-form__full">
                            <div class="price-table-form__item">
                                <label class="price-table-form-label">
                                    <span class="price-table-form-label__title">Tell us about your business — and we'll name it</span>
                                    <textarea type="text" rows="6" name="description"
                                              class="price-table-form__field price-table-form__input"></textarea>
                                </label>
                            </div>
                        </div>
                    </form>
<!--                    <script-->
<!--                            src="https://www.paypal.com/sdk/js?client-id=AYezyCwOGvrxXj7YVdYf6r_xDw9s5dxT561q4t1rux5WtVi6EIpKpDu9lX5lWgZnuhMUdfcN8laLfvUe">-->
<!--                    </script>-->
                    <script
                            src="https://www.paypal.com/sdk/js?client-id=<?php echo get_field('payment_client_id', 'options') ?>">
                    </script>

                    <div id="paypal-button-container"></div>
                </div>


            </div>
        </div>
    </div>
</div>

<div class="modal modal-thanks">
    <div class="modal-container">
        <div class="modal-content">
            <div class="modal-body">
                <span class="modal__close">×</span>
                <div class="modal-thanks-body">
                    <div class="success-icon modal-thanks-body__icon">

                    </div>
                    <p class="modal-thanks__text">Thank you for trusting Candletask to provide you with awesome name
                        ideas
                        for your company.</p>
                    <p class="modal-thanks__text"><strong>We’ll send your names in <span class="modal-price-head__time"></span> hours.</strong></p>
                    <p class="modal-thanks__text">If you do not receive an email from us within that time frame, be sure
                        to
                        check your spam folder.</p>
                    <p class="modal-thanks__text"> Feel free to contact us at <a href="mailto:info@candletsk.com">info@candletsk.com</a>
                        with any questions or
                        concerns.</p>
                </div>
                <div class="modal-thanks__footer">
                    <span class="modal-body__ok">ok</span>
                </div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" id="paypalPrice">




